#How to prepare the EGB Demo

Hardware requirements:
- Netgear Router
- RaspberryPi Zero / W
- RaspberryPi 3B
- RaspberryPi 4 8GB
- SD Cars (minimum size 8GB)
- PiFace Digital
- PiCamera
- Ultrasonic Sensor
- WiFi Dongle
- Kasa Smart Plugs (Plug 1-3)

#SD Card Preparation
1. Use a empty SD Card for the image. Download the official __Raspberry Pi OS Lite__ image from this site: [raspberrypi.org](https://www.raspberrypi.org/software/operating-systems/).

2. Flash the image to the SD Card (Use [Balena Etcher](https://github.com/balena-io/etcher/releases/tag/v1.6.0) for example).

3. Eject the SD Card and re-insert it directly again into your PC.

4. Go to the SD Card (boot partition) and copy the files SSH and wpa-supplicant.conf to the SD Card.

5. Edit the wpa-supplicant.conf file to your preferences.

6. Eject the SD Card and put it into your PI + Boot the system and login to the PI. (user: pi, password: raspberry).

7. Update the system:
    sudo apt-get update
    sudo apt-get upgrade -y

8. Edit your raspberrypi specific settings and preferences:
    sudo raspi-config

9. Install npm/node.js
- Determine what architecture the pi is running and download the specific node.js version
    uname -m

- For armv61 Version:
    wget https://unofficial-builds.nodejs.org/download/release/v14.10.0/node-v14.10.0-linux-armv6l.tar.gz

- For other version consult node.js website: [Node.js](https://nodejs.org/en/download/)
    
- Do the following with the downloaded package then (replace your file names!):
    tar -xzf node-v14.10.0-linux-armv6l.tar.gz
    cd node-v14.10.0-linux-armv6l/
    sudo cp -R * /usr/local/

- Check if installation was successfull:
    node -v
    npm -v

10. Install NodeRed:
    bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered) --node14

11. Enable node red service:
    sudo systemctl enable nodered.service
    sudo systemctl start nodered.service

12. Install mosquito:
    sudo apt-get install mosquitto mosquitto-clients -y

#Open Node Red on the raspberry pi
1. Use your browser and open the node red installation via the port 1880: http://XXX.XXX.XXX.XXX:1880

#Setup of the TP Link Kasa Smart Plugs
1. Download the TP Link Kasa App from the AppStore oder PlayStore.

2. Follow the setup instructions of the App and use the Network, which you set up with the router.

3. Import the emeter-demo.json to the Node Red environment and follow the next installation/connection instructions

#Setup of Azure IoT Hub connection with Node Red
Follow the installation instructions, [here](https://flows.nodered.org/node/node-red-contrib-azure-iot-hub)

#Setup of Power BI Flow with Live Data visualization
Follow the installation instructions, [here](https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-live-data-visualization-in-power-bi)


#IoT Edge Case with the Azure IoT Hub
Follow the installation instructions, [here](https://docs.microsoft.com/en-us/azure/iot-edge/how-to-install-iot-edge?view=iotedge-2020-11)